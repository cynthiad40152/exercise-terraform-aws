# Specify the provider and access details

provider "aws" {
  access_key = "${var.aws_access_key}"
  secret_key = "${var.aws_secret_key}"
  region = "${var.aws_region}"
}

#VPC1

resource "aws_vpc" "VPC1" {
  cidr_block = "10.0.0.0/16"
}

resource "aws_route_table" "r1" {
  vpc_id = "${aws_vpc.VPC1.id}"

route {
  cidr_block = "0.0.0.0/0"
  gateway_id = "${aws_internet_gateway.gateway-vpc1.id}"
 }
}

#VPC2

resource "aws_vpc" "VPC2" {
  cidr_block = "192.160.0.0/16"
}

resource "aws_route_table" "r2" {
  vpc_id = "${aws_vpc.VPC2.id}"
}
#PEERING CONNECTION

resource "aws_vpc_peering_connection" "VPC1_VPC2" {
  peer_vpc_id   = "${aws_vpc.VPC2.id}"
  vpc_id        = "${aws_vpc.VPC1.id}"
  auto_accept   = true
}

#SUBNET

resource "aws_subnet" "subnet1" {
  vpc_id     = "${aws_vpc.VPC1.id}"
  cidr_block = "10.0.0.0/16"
  map_public_ip_on_launch = true
}

resource "aws_subnet" "subnet2" {
  vpc_id     = "${aws_vpc.VPC2.id}"
  cidr_block = "192.160.0.0/16"
}

#GATEWAY

resource "aws_internet_gateway" "gateway-vpc1" {
  vpc_id = "${aws_vpc.VPC1.id}"
}

#SECURITY GROUP

resource "aws_security_group" "sg1" {
  name        = "sg"
  description = "Allow all inbound traffic"
  vpc_id      = "${aws_vpc.VPC1.id}"

  ingress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
  }

  egress {
    from_port       = 0
    to_port         = 0
    protocol        = "-1"
  }
}

resource "aws_security_group" "sg2" {
  name        = "sg"
  description = "Allow all inbound traffic"
  vpc_id      = "${aws_vpc.VPC2.id}"

  ingress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
  }

  egress {
    from_port       = 0
    to_port         = 0
    protocol        = "-1"
  }
}

#INSTANCES

resource "aws_instance" "vpc1_1" {
  ami           = "${var.ami}"
  instance_type = "t2.micro"
  subnet_id = "${aws_subnet.subnet1.id}"
  private_ip = "10.0.0.12"
  key_name = "mykeypair"
}

resource "aws_instance" "vpc1_2" {
  ami           = "${var.ami}"
  instance_type = "t2.micro"
  subnet_id = "${aws_subnet.subnet1.id}"
  private_ip = "10.0.0.13"
  key_name = "mykeypair"
}

resource "aws_instance" "vpc2_1" {
  ami           = "${var.ami}"
  instance_type = "t2.micro"
  subnet_id = "${aws_subnet.subnet2.id}"
  private_ip = "192.160.0.12"
  key_name = "mykeypair"
}

resource "aws_instance" "vpc2_2" {
  ami           = "${var.ami}"
  instance_type = "t2.micro"
  subnet_id = "${aws_subnet.subnet2.id}"
  private_ip = "192.160.0.13"
  key_name = "mykeypair"
}

resource "aws_eip" "instance1-vpc1" {
  instance = "${aws_instance.vpc1_1.id}"
  vpc      = true
  depends_on = ["aws_internet_gateway.gateway-vpc1"]
}
